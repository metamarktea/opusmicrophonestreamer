# OpusMicrophoneStreamer

A simple C# HTTP server that will take your microphone and encode it into OPUS, then send that open out as a OGG live stream.

RFC: https://datatracker.ietf.org/doc/html/rfc6716
RFC: https://www.rfc-editor.org/rfc/rfc7845.html

## Getting started
Open the project in Visual Studio 2022
Compile and Run.

You may need to open port 80 for this to work:
If you get Access Denied you will need to run:
```cmd
netsh http add urlacl url=http://+:80/ user=SomeUserNameOrLocalDomainAndAccount@live.com
```

## Name
Opus Microphone Streamer

## Description
The most difficult part was to get the CRC32 correct, there are apparantly many versions, this one works nicely!

## Roadmap
This is a for fun personal project, if its useful to you let me know! =D

## Authors and acknowledgment
Meta Mark Tea

## License
MIT

