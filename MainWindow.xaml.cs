﻿using NAudio.Wave;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MicrophoneStreamer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public class OutputStream
        {
            public HttpListener Listener { get; set; }
            public Stream Stream { get; set; }

            public HttpListenerRequest Request { get; set; }
            public HttpListenerResponse Response { get; set; }
            public uint seq = 1;
            public uint time = 1;
        }

        Thread listenThread;
        ObservableCollection<ListBoxItem> uriList = new ObservableCollection<ListBoxItem>();
        WaveInEvent microphone;
        OpusDotNet.OpusEncoder encoder;

        byte[] _notEncodedBuffer = new byte[0];

        ConcurrentQueue<OutputStream> responseStreams = new ConcurrentQueue<OutputStream>();


        private void WaveIn_DataAvailable(object? sender, WaveInEventArgs args)
        {
            WaveInEventArgs e = args;

            float maxValue = 32767;
            int peakValue = 0;
            int bytesPerSample = 2;
            for (int index = 0; index < args.BytesRecorded; index += bytesPerSample)
            {
                int value = BitConverter.ToInt16(args.Buffer, index);
                peakValue = Math.Max(peakValue, value);
            }

            lock (encoder)
            {
                uint samples = (uint)(e.BytesRecorded / bytesPerSample);

                byte[] soundBuffer = new byte[e.BytesRecorded + _notEncodedBuffer.Length];
                for (int i = 0; i < _notEncodedBuffer.Length; i++)
                {
                    soundBuffer[i] = _notEncodedBuffer[i];
                }
                for (int i = 0; i < e.BytesRecorded; i++)
                {
                    soundBuffer[i + _notEncodedBuffer.Length] = e.Buffer[i];
                }

                int byteCap = e.BytesRecorded / bytesPerSample;
                int segmentCount = (int)Math.Floor((decimal)soundBuffer.Length / byteCap);
                int segmentsEnd = segmentCount * byteCap;
                int notEncodedCount = soundBuffer.Length - segmentsEnd;
                _notEncodedBuffer = new byte[notEncodedCount];
                for (int i = 0; i < notEncodedCount; i++)
                {
                    _notEncodedBuffer[i] = soundBuffer[segmentsEnd + i];
                }

                for (int i = 0; i < segmentCount; i++)
                {
                    byte[] segment = new byte[byteCap];
                    for (int j = 0; j < segment.Length; j++)
                    {
                        segment[j] = soundBuffer[(i * byteCap) + j];
                    }

                    byte[] opusDayBuffer = new byte[byteCap];
                    int encodedBytes = encoder.Encode(segment, segment.Length, opusDayBuffer, opusDayBuffer.Length);
                    opusDayBuffer = opusDayBuffer.Take(encodedBytes).ToArray();

                    foreach (var response in responseStreams)
                    {
                        byte[] packet = new byte[encodedBytes + 12];
                        opusDayBuffer.CopyTo(packet, 12);


                        OggPacket oggPage = new OggPacket();
                        oggPage.packet = opusDayBuffer;
                        oggPage.bytes = encodedBytes;
                        oggPage.granulepos = response.time;
                        oggPage.packetno = response.seq;
                        oggPage.cont = false;
                        byte[] packetData = oggPage.ToBinary();


                        response.Stream.Write(packetData, 0, packetData.Length);

                        response.time += samples;
                        response.seq++;
                    }
                }
            }

            Dispatcher.BeginInvoke(() =>
            {
                soundBar.Maximum = maxValue;
                soundBar.Value = peakValue;
            });

        }

        public MainWindow()
        {
            InitializeComponent();

            listBox.ItemsSource = uriList;
            encoder = new OpusDotNet.OpusEncoder(OpusDotNet.Application.Audio, 48000, 1);

            NAudio.CoreAudioApi.MMDeviceEnumerator enumer = new NAudio.CoreAudioApi.MMDeviceEnumerator();
            foreach (var device in enumer.EnumerateAudioEndPoints(NAudio.CoreAudioApi.DataFlow.Capture, NAudio.CoreAudioApi.DeviceState.Active))
            {
                uriList.Add(new ListBoxItem() { Content = device.FriendlyName });
            }

            microphone = new WaveInEvent
            {
                DeviceNumber = 0, // customize this to select your microphone device
                WaveFormat = new WaveFormat(rate: 48000, bits: 16, channels: 1),
                BufferMilliseconds = 20
            };


            microphone.DataAvailable += WaveIn_DataAvailable;
            microphone.StartRecording();


            listenThread = new Thread(() =>
           {
               HttpListener listener = new HttpListener();
               listener.IgnoreWriteExceptions = true;
               listener.Prefixes.Add("http://+:80/");

               string hostName = Dns.GetHostName();
               IPHostEntry ipEntry = Dns.GetHostEntry(hostName);
               IPAddress[] addr = ipEntry.AddressList;

               for (int i = 0; i < addr.Length; i++)
               {
                   if (addr[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                   {
                       listener.Prefixes.Add($"http://{addr[i]}:80/");
                   };
               }
               
               listener.Start();

               while (true)
               {
                   Console.WriteLine("Listening...");
                   // Note: The GetContext method blocks while waiting for a request.
                   HttpListenerContext context = listener.GetContext();
                   HttpListenerRequest request = context.Request;
                   HttpListenerResponse response = context.Response;

                   string responseString = @"
                    <HTML>
                        <BODY> 
                            <!--video controls autoplay>
                                <source src=""?stream=mic.opus"" type=""audio/ogg"">
                            </video-->
                            <br/>
                            <audio autoplay controls>
                                <source src=""?stream=mic.opus""/>    
                            </audio>
                        </BODY>
                    </HTML>";

                   Dispatcher.BeginInvoke(() =>
                   {
                       uriList.Add(new ListBoxItem() { Content = request.RawUrl });
                   }
                  );

                   byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);

                   if (request.QueryString["stream"] == "mic.opus")
                   {

                       // Get a response stream and write the response to it.                       
                       //response.SendChunked = false;
                       response.ContentType = "audio/ogg";
                       response.Headers.Add("Cache-Control", "no-cache, no-store");

                       Stream output = response.OutputStream;

                       var oggHeader = Opus.CreateOpusHeader(48000, 1);
                       var oggTags = Opus.CreateOpusTags();

                       output.Write(oggHeader.ToBinary());
                       output.Write(oggTags.ToBinary());

                       responseStreams.Enqueue(new OutputStream()
                       {
                           Listener = listener,
                           Stream = output,
                           Request = request,
                           Response = response,
                       });

                   }
                   else
                   {
                       // Get a response stream and write the response to it.
                       response.ContentLength64 = buffer.Length;
                       System.IO.Stream output = response.OutputStream;
                       output.Write(buffer, 0, buffer.Length);
                       output.Close();
                   }
               }

           });

            listenThread.IsBackground = true;
            listenThread.Start();

        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
