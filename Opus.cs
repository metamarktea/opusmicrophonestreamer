﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicrophoneStreamer
{
    public class Opus
    {
        /* manufacture a generic OpusHead packet */
        public static OggPacket CreateOpusHeader(int samplerate, byte channels)
        {
            int size = 19;

            byte[] data = new byte[size];
            BinaryWriter writer = new BinaryWriter(new MemoryStream(data));
            writer.Write(Encoding.UTF8.GetBytes("OpusHead"));
            writer.Write((byte)1);
            writer.Write((byte)1);
            writer.Write((ushort)20);
            writer.Write((uint)samplerate);
            writer.Write((short)0); // gain
            writer.Write((byte)0); // mapping 0 covers mono or stereo

            OggPacket op;
            op.packet = data;
            op.bytes = size;
            op.cont = true;
            op.b_o_s = true;
            op.e_o_s = false;
            op.granulepos = 0;
            op.packetno = 0;

            return op;
        }

        /* manufacture a generic OpusTags packet */
        public static OggPacket CreateOpusTags()
        {
            string vendor = "MetaMarkTea";
            int vendorLength = vendor.Length;
            int size = 8 + 4 + vendor.Length + 4;

            byte[] data = new byte[size];
            BinaryWriter writer = new BinaryWriter(new MemoryStream(data)); /* identifier */
            writer.Write(Encoding.UTF8.GetBytes("OpusTags"));
            writer.Write((int)vendorLength);
            writer.Write(Encoding.UTF8.GetBytes(vendor));
            writer.Write((int)0); // Custom Tags

            OggPacket op;
            op.packet = data;
            op.bytes = size;
            op.b_o_s = false;
            op.e_o_s = false;
            op.granulepos = 0;
            op.packetno = 0;
            op.cont = false;

            return op;
        }

    }
}
