﻿using System;
using System.Linq;
namespace CRC
{


    /*
Simplified BSD License

Copyright 2017 Derek Will

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE.
*/


    public class Crc
    {

        /// <summary>
        /// Gets the CRC algorithm parameters.
        /// </summary>
        /// <value>The CRC algorithm parameters.</value>
        public CrcParameters Parameters { get; private set; }

        /// <summary>
        /// Gets the lookup table used in calculating check values.
        /// </summary>
        /// <value>The lookup table.</value>
        public ulong[] LookupTable { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrcSharp.Crc"/> class.
        /// </summary>
        /// <param name="parameters">CRC algorithm parameters.</param>
        public Crc(CrcParameters parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException("parameters", "Parameters argument cannot be null.");

            Parameters = parameters;
            LookupTable = GenerateLookupTable();
        }

        /// <summary>
        /// Calculates the CRC check value as a numeric value.
        /// </summary>
        /// <returns>The CRC check value as a numeric value.</returns>
        /// <param name="data">Data to compute the check value of.</param>
        public ulong CalculateAsNumeric(byte[] data)
        {
            byte[] crcCheckVal = CalculateCheckValue(data);
            Array.Resize(ref crcCheckVal, 8);
            return BitConverter.ToUInt64(crcCheckVal, 0);
        }

        /// <summary>
        /// Calculates the CRC check value as a byte array.
        /// </summary>
        /// <returns>The CRC check value as a byte array.</returns>
        /// <param name="data">Data to compute the check value of.</param>
        public byte[] CalculateCheckValue(byte[] data)
        {
            if (data == null)
                throw new ArgumentNullException("data", "Data argument cannot be null.");

            ulong crc = Parameters.InitialValue;

            if (Parameters.ReflectIn)
            {
                crc = ReflectBits(crc, Parameters.Width);
            }

            foreach (byte b in data)
            {
                if (Parameters.ReflectIn)
                {
                    crc = LookupTable[(crc ^ b) & 0xFF] ^ (crc >> 8);
                }
                else
                {
                    crc = LookupTable[((crc >> (Parameters.Width - 8)) ^ b) & 0xFF] ^ (crc << 8);
                }

                crc &= (UInt64.MaxValue >> (64 - Parameters.Width));
            }

            // Source: https://stackoverflow.com/questions/28656471/how-to-configure-calculation-of-crc-table/28661073#28661073
            // Per Mark Adler - ...the reflect out different from the reflect in (CRC-12/3GPP). 
            // In that one case, you need to bit reverse the output since the input is not reflected, but the output is.
            if (Parameters.ReflectIn ^ Parameters.ReflectOut)
            {
                crc = ReflectBits(crc, Parameters.Width);
            }

            ulong crcFinalValue = crc ^ Parameters.XorOutValue;
            return BitConverter.GetBytes(crcFinalValue).Take((Parameters.Width + 7) / 8).ToArray();
        }

        /// <summary>
        /// Generates the lookup table using the CRC algorithm parameters.
        /// </summary>
        /// <returns>The lookup table.</returns>
        private ulong[] GenerateLookupTable()
        {
            if (Parameters == null)
                throw new InvalidOperationException("CRC parameters must be set prior to calling this method.");

            var lookupTable = new ulong[256];
            ulong topBit = (ulong)((ulong)1 << (Parameters.Width - 1));

            for (int i = 0; i < lookupTable.Length; i++)
            {
                byte inByte = (byte)i;
                if (Parameters.ReflectIn)
                {
                    inByte = (byte)ReflectBits(inByte, 8);
                }

                ulong r = (ulong)((ulong)inByte << (Parameters.Width - 8));
                for (int j = 0; j < 8; j++)
                {
                    if ((r & topBit) != 0)
                    {
                        r = ((r << 1) ^ Parameters.Polynomial);
                    }
                    else
                    {
                        r = (r << 1);
                    }
                }

                if (Parameters.ReflectIn)
                {
                    r = ReflectBits(r, Parameters.Width);
                }

                lookupTable[i] = r & (UInt64.MaxValue >> (64 - Parameters.Width));
            }

            return lookupTable;
        }

        /// <summary>
        /// Reflects the bits of a provided numeric value.
        /// </summary>
        /// <returns>Bit-reflected version of the provided numeric value.</returns>
        /// <param name="b">Value to reflect the bits of.</param>
        /// <param name="bitCount">Number of bits in the provided value.</param>
        private static ulong ReflectBits(ulong b, int bitCount)
        {
            ulong reflection = 0x00;

            for (int bitNumber = 0; bitNumber < bitCount; ++bitNumber)
            {
                if (((b >> bitNumber) & 0x01) == 0x01)
                {
                    reflection |= (ulong)(((ulong)1 << ((bitCount - 1) - bitNumber)));
                }
            }

            return reflection;
        }
    }

    /// <summary>
    /// CRC algorithm parameters.
    /// </summary>
    public class CrcParameters
    {                                                
        /// <summary>
        /// Gets the width of the CRC algorithm in bits.
        /// </summary>
        /// <value>The width of the CRC algorithm in bits.</value>
        public int Width { get; private set; }        

        /// <summary>
        /// Gets the polynomial of the CRC algorithm.
        /// </summary>
        /// <value>The polynomial of the CRC algorithm.</value>
        public ulong Polynomial { get; private set; }

        /// <summary>
        /// Gets the initial value used in the computation of the CRC check value.
        /// </summary>
        /// <value>The initial value used in the computation of the CRC check value.</value>
        public ulong InitialValue { get; private set; }

        /// <summary>
        /// Gets the value which is XORed to the final computed value before returning the check value.
        /// </summary>
        /// <value>The value which is XORed to the final computed value before returning the check value.</value>
        public ulong XorOutValue { get; private set; }

        /// <summary>
        /// Gets a value indicating whether bytes are reflected before being processed.
        /// </summary>
        /// <value><c>true</c> if each byte is to be reflected before being processed; otherwise, <c>false</c>.</value>
        public bool ReflectIn { get; private set; }

        /// <summary>
        /// Gets a value indicating whether the final computed value is reflected before the XOR stage.
        /// </summary>
        /// <value><c>true</c> if the final computed value is reflected before the XOR stage; otherwise, <c>false</c>.</value>
        public bool ReflectOut { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="CrcSharp.CrcParameters"/> class.
        /// </summary>
        /// <param name="width">Width of the CRC algorithm in bits.</param>
        /// <param name="polynomial">Polynomial of the CRC algorithm.</param>
        /// <param name="initialValue">Initial value used in the computation of the CRC check value.</param>
        /// <param name="xorOutValue">The value which is XORed to the final computed value before returning the check value.</param>
        /// <param name="reflectIn">If set to <c>true</c> each byte is to be reflected before being processed.</param>
        /// <param name="reflectOut">If set to <c>true</c> the final computed value is reflected before the XOR stage.</param>
        public CrcParameters(int width, ulong polynomial, ulong initialValue, ulong xorOutValue, bool reflectIn, bool reflectOut)
        {
            ThrowIfParametersInvalid(width, polynomial, initialValue, xorOutValue);

            Width = width;
            Polynomial = polynomial;
            InitialValue = initialValue;
            XorOutValue = xorOutValue;
            ReflectIn = reflectIn;
            ReflectOut = reflectOut;
        }

        /// <summary>
        /// Verifies if the parameter values are valid.
        /// </summary>
        /// <param name="width">Width of the CRC algorithm in bits.</param>
        /// <param name="polynomial">Polynomial of the CRC algorithm.</param>
        /// <param name="initialValue">Initial value used in the computation of the CRC check value.</param>
        /// <param name="xorOutValue">The value which is XORed to the final computed value before returning the check value.</param>
        private void ThrowIfParametersInvalid(int width, ulong polynomial, ulong initialValue, ulong xorOutValue)
        {
            if (width < 8 || width > 64)
                throw new ArgumentOutOfRangeException("width", "Width must be between 8-64 bits.");

            ulong maxValue = (UInt64.MaxValue >> (64 - width));

            if (polynomial > maxValue)
                throw new ArgumentOutOfRangeException("polynomial", string.Format("Polynomial exceeds {0} bits.", width));

            if (initialValue > maxValue)
                throw new ArgumentOutOfRangeException("initialValue", string.Format("Initial Value exceeds {0} bits.", width));

            if (xorOutValue > maxValue)
                throw new ArgumentOutOfRangeException("xorOutValue", string.Format("XOR Out Value exceeds {0} bits.", width));
        }
    }
}


