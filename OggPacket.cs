﻿using System.IO;
using System.Text;

namespace MicrophoneStreamer
{

    public struct OggPacket
    {
        public byte[] packet;
        public int bytes;
        public bool b_o_s;
        public bool e_o_s;
        public bool cont;

        public long granulepos;
        public long packetno;

        public byte[] ToBinary()
        {
            /* 
             * Based on 
             * https://github.com/pion/webrtc/blob/master/pkg/media/oggwriter/oggwriter.go
             * https://www.ietf.org/rfc/rfc3533.txt
             * 
            */

            var segmentTable = new byte[1]; /* segment table stores segment length map. always providing one single segment */
            segmentTable = new byte[bytes / 255 + 1];

            for (int i = 0; i < segmentTable.Length - 1; i++)
            {
                segmentTable[i] = 255;
            }

            segmentTable[segmentTable.Length - 1] = (byte)(bytes % 255);


            MemoryStream memory = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memory);
            writer.Write(Encoding.UTF8.GetBytes("OggS"));
            writer.Write((byte)0); // version


            if (b_o_s)
            {
                writer.Write((byte)2);
            }
            else if (e_o_s)
            {
                writer.Write((byte)4);
            }
            else
            {
                writer.Write((byte)(cont ? 1 : 0)); // 1 = continuation, 2 = beginning of stream, 4 = end of stream
            }

            writer.Write((long)granulepos); // 8 bytes granule
            writer.Write((int)0); // 4 byte SN
            writer.Write((int)packetno); // 4 byte packet number
            writer.Write((uint)0); // THE CRC filled with zero, filled in after the crc.
            writer.Write((byte)segmentTable.Length);
            writer.Write(segmentTable, 0, segmentTable.Length);
            writer.Write(packet, 0, bytes);

            byte[] data = memory.ToArray();
            var k = new CRC.Crc(new CRC.CrcParameters(32, 0x04c11db7, 0, 0, false, false));
            byte[] finalAnswer = k.CalculateCheckValue(data);

            // put the crc back in
            memory.Position = 22;
            writer.Write(finalAnswer);

            data = memory.ToArray();
            return data;
        }
    }
}
